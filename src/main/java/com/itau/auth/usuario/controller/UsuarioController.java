package com.itau.auth.usuario.controller;

import com.itau.auth.usuario.model.Usuario;
import com.itau.auth.usuario.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.HashMap;
import java.util.Map;

@RestController
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;

    @GetMapping("/me")
    public Map<String, Object> validar(Principal principal) {
        Usuario usuario = usuarioService.findByNome(principal.getName());
        Map<String, Object> map = new HashMap<>();
        map.put("name", usuario.getNome());
        map.put("id", usuario.getId());
        return map;
    }
}
