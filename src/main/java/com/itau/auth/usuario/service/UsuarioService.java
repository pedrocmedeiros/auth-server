package com.itau.auth.usuario.service;

import com.itau.auth.usuario.model.Usuario;
import com.itau.auth.usuario.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class UsuarioService implements UserDetailsService {
    @Autowired
    private BCryptPasswordEncoder encoder;

    @Autowired
    private UsuarioRepository repository;

    @PostConstruct
    public void popular() {
        Usuario usuario = new Usuario();
        usuario.setId(1);
        usuario.setNome("Pedro");
        usuario.setSenha(encoder.encode("12345"));
        repository.save(usuario);
    }

    public Usuario findByNome(String nome) {
        return repository.findByNome(nome)
                .orElseThrow(() -> new UsernameNotFoundException(String.format("Usuario '%s' nao encontrado", nome)));
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Usuario byNome = findByNome(username);

        return User.builder()
                .username(byNome.getNome())
                .password(byNome.getSenha())
                .authorities(new SimpleGrantedAuthority("user"))
                .build();
    }
}
